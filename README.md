# ParaWeather
A clean web app that displays how good the current and forecasted weather is for paragliding.   

## Description
When is it good weather for paragliding?  Use this app to find out.  Currently there is are three custom algorithm implemented to analyze launch conditions at Lookout Mountian, GA and Ellenville, NY. 

## Technology
- To get weather data, paraweather uses open metoe's API using their free license: https://open-meteo.com/en/docs
- To format the webpage, paraweather uses Bootstrap.
- To serve the webpage, paraweather uses Python with the aiohttp and asyncio libraries.
- To generate the graphs, paraweather uses Python with the matplotlib library.


## Structure
- pw_constants.py holds all constants we will use.  
- weather_api.py holds all of the code for getting and formatting weather forecast data
- pg_engine.py holds all of the code for calculating how good the weather is
- pw_server holds the code for the web server that presents the interface
- pw.py runs the web server.
- index_begin.html and index_end.html hold the html for the single page in this web app.

## Future Work
To implement a new weather analysis algorithm, write a function that takes the weather data and lat/lon location as inputs, and reports a list of integer scores as the output.  Use the additive_simple() function in pg_engine.py as a template.  Then in pw.py add a check for that algorithm in get_analysis().  You must also add an entryto the algorithms dictionary in pw_constants.py.  Lastly, Add a checkbox to select that algorithm in index_begin.html.

Too add a new location, add an entry into the launch_sites dictionary in pw_constant, and add radio button in index_begin.html.  Note that altitude is measured in feet.


## Usage
python3 pw_server.py
Then navigate your web browser to this machine, at port 8080.  


