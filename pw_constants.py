# file fill of constants to use
# always pull from here

# Paragliding sites information.  Put them all in as dicts like this.

launch_sites = {
# lookout mountain flight park
"lmfp":{
    'name': "Lookout Mountain",
    'lat': 34.89642663587001,
    'lon': -85.44612565530562,
    'launch_directions': [310],
    'launch_altitude': 1300
},
"eville":{
    'name': "Ellenville",
    'lat': 41.67192262018279,
    'lon': -74.40324523125449,
    'launch_directions': [270, 310],
    'launch_altitude': 1100
},
}

algorithm_names = {"launch_optimistic":   "Optimistic",
                   "launch_pessimistic": "Pessimistic",
                   "launch_perfect": "Perfectionist",
                   "additive_simple": "Additive"}

