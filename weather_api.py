# All api calls will be here in this file
# Using https://open-meteo.com/
# craft your api call here: https://open-meteo.com/en/docs

import pw_constants as pw
import requests


# function to turn a float into a string.
# formatted with 4 decimal places because that is how open-meteo likes it
# input is float, output is string of the float with 4 decimal places
def format_lat_lon(latitude):
    lat_string = "{:.{}f}".format(latitude, 4)  # 4 decimal places
    return(lat_string)


# function to get the weather data for the given lat and lon.
# input: latitude(float)=latitude, longitude(float)=longitude, data_type(string, optional) = type of data to return
# data_type options: "standard" = a bunch of weather data, no other options implemented (yet ;))
# output: weather_Data, a dictionary containing all the weather data
def get_weather_data(latitude, longitude, data_type="standard"):
    base_url = "https://api.open-meteo.com/v1/forecast?"

    # format lat and lon for the api request
    lat_str = format_lat_lon(latitude)
    lon_str = format_lat_lon(longitude)
    location_data = "latitude=" + lat_str + "&longitude=" + lon_str

    # depending on data_type, select the proper query
    if data_type == "standard":
        data_query = "&hourly=temperature_2m,precipitation_probability,precipitation,visibility,windspeed_10m,windspeed_80m,windspeed_120m,windspeed_180m,winddirection_10m,winddirection_80m,winddirection_120m,winddirection_180m,windgusts_10m,direct_radiation,terrestrial_radiation&daily=sunrise,sunset,precipitation_hours,windspeed_10m_max,windgusts_10m_max,winddirection_10m_dominant&temperature_unit=fahrenheit&windspeed_unit=kn&precipitation_unit=inch&forecast_days=16&timezone=America%2FNew_York"
    # add other options for data_type here
    else:
        data_query = ""
        raise Exception("Unsupported data_type in get_weather_data")

    # perform the api call
    full_request = base_url+location_data+data_query
    weather_data = requests.get(url=full_request).json()

    return(weather_data)


def test():
    # use lmfp as the location for now
    lat = pw.lmfp['lat']
    lon = pw.lmfp['lon']
    print(get_weather_data(lat, lon))
    return()


